from flask import Flask, request, render_template
app = Flask(__name__)
application = app

@app.route('/')
def session():
    return render_template ('index.html')

@app.route('/args')
def args():
    return render_template ('task1.html')

@app.route('/cookies')
def cookies():
    return render_template ('task2.html')

@app.route('/forms', methods=["GET","POST"])
def forms():
    return render_template ('task3.html')