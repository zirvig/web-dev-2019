from flask import Flask, render_template, request
app = Flask(__name__)
application = app

@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/calculator')
def calc():
    res = None
    msg = None
    if request.args:
        regex = r'^\d+(\.\+)?$'
        n1 = request.args.get('n1')
        n2 = float(request.args.get('n2'))
        op = request.args.get('op')
        try:
            n1 = float(n1)
            n2 = float(n2)
            if op == '+':
                res = n1 + n2
            elif op == '-':
                res = n1 - n2
            elif op == '*':
                res = n1 * n2
            elif op == '/':
                res = n1 / n2
        except ZeroDivisionError:
            msg = 'Error: undefined result'
        except ValueError:
            msg = "You should enter only digits"
        except Exception as e:
            msg = 'Unexpected error of type e.__class__.__name__'
    return render_template ('calculator.html', res = res, msg = msg)