from flask import Flask, request, render_template
from collections import Counter
import re
app = Flask(__name__)
application = app

@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/glavnaya')
def glavnaya():
    return render_template ('glavnaya.html')

@app.route('/phonenumber', methods=['GET','POST'])
def phonenumber():
    phonenumber = request.form.get('phonenumber', '')
    digits = ''
    err = ''
    formatted = ''
    if phonenumber:
        for c in phonenumber:
            if c.isdigit():
                digits += c
            elif c == '(' or c == ')' or c == '-' or c =='.' or c == '+' or c == ' ':
                continue
            else:
                err = 'Используйте только цифры, скобки, дефисы и точки'
                break
        if len(digits) >= 10 and len(digits) <= 11:
                err = 'Номер должен состоять из 10 или 11 цифр.\n'
        if len(digits) == 10:
                digits = '8' + digits
                prefix = '+7'
        if not err:
            formatted = f'{digits[-10]}-{digits[-10:-7]}-{digits[-7:-4]}-{digits[-4:-2]}-{digits[-2:]}'
    return render_template('phonenumber.html', phonenumber=phonenumber, formatted=formatted, err=err)

@app.route('/analiz', methods=['GET','POST'])
def analiz():
    text = request.form.get('text', '')
    c = Counter()
    if text:
        c['text_len'] = len(text)
        for char in text:
            if char == ' ':
                c['words'] += 1
            elif char in '.!?':
                c['sentences'] += 1
                c['words'] += 1
            elif char.isalnum():
                c['digits_and_nums'] += 1
            elif char == '\n':
                c['paragraphs'] += 1
        c['re_digits_and_nums'] = len(re.findall(r'\w', text))
        c['re_words'] = len(re.findall(r'\w+', text))
        c['re_paragraphs'] = len(re.findall(r'\n+', text))
        c['re_sentences'] = len(re.findall(r'[\.!\?]', text))
    return render_template ('analiz.html', text=text, c=c)

@app.route('/authentication')
def authentication():
    return render_template ('authentication.html')