from flask import Flask, request, render_template, session, redirect, url_for
from mysql_db import MySQL
import flask_login

app = Flask(__name__)
application = app

app.config.from_pyfile('config.py')

mysql = MySQL(app)

#import auth

@app.route('/')
def hello_world():
    cursor = mysql.connection().cursor(named_tuple=True)
    cursor.execute('select * from users;')
    users = cursor.fetchall()
    cursor.close()
    return render_template('index.html', users=users)

@app.route('/users/<int:id>')
def show(id):
    cursor = mysql.connection().cursor(named_tuple=True)
    cursor.execute('select * from users where id=%s;', (id,))
    user = cursor.fetchone()
    cursor.close()
    return render_template('show.html', user=user)